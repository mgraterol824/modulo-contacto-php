<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Contacto</title>
  <link rel="stylesheet" href="../static/css/bootstrap.css">
  <link rel="stylesheet" href="../static/css/custom.css">
</head>
<body>
  
  <main class="container-fluid" id="sended">
    <nav class="navbar navbar-dark justify-content-sm-center justify-content-md-center" style="height: 2.6rem;">      
    </nav>

    
    <div class="text-center m-5">
      <p>Su solicitud ha sido enviada</p>
      <p>Pronto nos pondremos en contacto con usted</p>
    </div>

  </main>



  <script>
    setTimeout(function(){
      window.location.href = "contact.php";
    }, 5000);
  </script>
  <script src="../static/js/jquery3.4.1.min.js"></script>
  <script src="../static/js/bootstrap.min.js"></script>
</body>
</html>