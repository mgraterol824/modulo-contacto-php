<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Contacto</title>
  <link rel="stylesheet" href="../static/css/bootstrap.css">
  <link rel="stylesheet" href="../static/css/custom.css">
</head>
<body>
  
  <main class="container-fluid" id="contact">
    <nav class="navbar navbar-dark justify-content-sm-center justify-content-md-center">      
      <ul class="nav m-auto">
        <li class="nav-item">
          <a class="nav-link active" href="#" style="color:black;">
            CONTACTO
          </a>
        </li>
      </ul>
    </nav>
    <form class="form-group col-sm-12 col-md-8 m-auto mt-md-5 jumbotron" action="../controllers/ContactController.php" method="POST">
      <div class="form-row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="name">Nombre</label>
            <input class="form-control" id="name" name="name" type="text" maxlength="20" required>
          </div>
          <div class="form-group">
            <label for="email">Correo</label>
            <input class="form-control" id="email" name="email" type="email" name="email" maxlength="30" required>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="comment">Comentario</label>
            <textarea class="form-control" id="comment" name="comment" maxlength="250" cols="30" rows="5"></textarea>
          </div>
        </div>
        <div class="col-12 text-center">
          <button type="submit" class="btn btn-primary">Solicitar</button>
        </div>
      </div>
    </form>
  </main>

  <script src="../static/js/jquery3.4.1.min.js"></script>
  <script src="../static/js/bootstrap.min.js"></script>
</body>
</html>